# Rock-Paper-Sissors Game #

![Program Screenshot](./leerps.jpg)

A simple Rock-Paper-Sissors game using the command line. Built using ncurses and the C standard library.

## Building ##

Very straight forward method of building. If using Windows, just use this command:

<code>gcc main.c -lncurses -o lpscissors</code>
