/*
    A simple Rock, Paper, Sissors game
    Copyright (C) 2022  Lee Schroeder

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>

#define CHOICE_ROCK    1
#define CHOICE_PAPER   2
#define CHOICE_SISSORS 3

#define LOW_CHOICE 1
#define UP_CHOICE  3

int draw_main_menu(void)
{
    WINDOW *menu = newwin(6, 30, 5, 0);
    WINDOW *header = newwin(5, 30, 0, 0);
    WINDOW *footer = newwin(3, 30, 11, 0);
    int choice = 0;

    /* Refresh everything first so the windows can be shown */
    refresh();

    /* Draw the borders for the window */
    box(menu, 0, 0);
    box(header, 0, 0);
    box(footer, 0, 0);

    /* Draw the contents for the header */
    mvwprintw(header, 1, 1, "Rock-Paper-Sissors Game");
    mvwprintw(header, 2, 1, "Written by Lee Schroeder");
    mvwprintw(header, 3, 1, "Licensed under the GNU GPLv3");

    /* Draws the options for the player to choose from */
    mvwprintw(menu, 1, 1, "Main Menu");
    mvwprintw(menu, 2, 1, "=============");
    mvwprintw(menu, 3, 1, "1) Play Game");
    mvwprintw(menu, 4, 1, "2) Exit Game");

    /* Lets the user know what to do */
    mvwprintw(footer, 1, 1, "Choose one> ");

    /* Refresh the window with the new content */
    wrefresh(header);
    wrefresh(menu);
    wrefresh(footer);

    /* Get input from the user */
    choice = getch();

    /* Get rid of the border */
    wborder(header, ' ', ' ', ' ',' ',' ',' ',' ',' ');
    wborder(menu, ' ', ' ', ' ',' ',' ',' ',' ',' ');
    wborder(footer, ' ', ' ', ' ',' ',' ',' ',' ',' ');

    wrefresh(header);
    wrefresh(menu);
    wrefresh(footer);

    /* Get rid of the window if it's not being used. It helps
     * to prevent clutter when playing the game */
    delwin(header);
    delwin(menu);
    delwin(footer);

    /* Determine what the user chose */
    if ((char)choice == '1')
        return 1;
    else if ((char)choice == '2')
        return 2;

    /* If the choice is invalid */
    return 0;
}

int determine_fate(int input)
{
    WINDOW *fate = newwin(5, 22, 5, 2);
    int ai_choice = (rand() % (UP_CHOICE + 1 - LOW_CHOICE)) + LOW_CHOICE;

    refresh();

    box(fate, 0, 0);

    /* Make sure the choice is within range */
    if ((ai_choice <= (LOW_CHOICE-1)) || (ai_choice > UP_CHOICE))
        return 1;

    /* Let the user know what they chose */
    switch(input)
    {
        case CHOICE_ROCK:
        {
            mvwprintw(fate, 1, 1, "You Chose Rock!");
            break;
        }
        case CHOICE_PAPER:
        {
            mvwprintw(fate, 1, 1, "You Chose Paper!");
            break;
        }
        case CHOICE_SISSORS:
        {
            mvwprintw(fate, 1, 1, "You Chose Sissors!");
            break;
        }
        default:
        {
            /* Shouldn't get here, but we have to consider it anyways */
            mvwprintw(fate, 1, 1, "Invalid Option!");
            break;
        }
    }

    /* Let the user know what the computer chose */
    switch(ai_choice)
    {
        case CHOICE_ROCK:
        {
            mvwprintw(fate, 2, 1, "They Chose Rock!");
            break;
        }
        case CHOICE_PAPER:
        {
            mvwprintw(fate, 2, 1, "They Chose Paper!");
            break;
        }
        case CHOICE_SISSORS:
        {
            mvwprintw(fate, 2, 1, "They Chose Scissors!");
            break;
        }
        default:
        {
            /* Shouldn't get here, but we have to consider it anyways */
            mvwprintw(fate, 2, 1, "Invalid Option!");
            break;
        }
    }

    /* If it's a tie */
    if (((input == CHOICE_ROCK) && (ai_choice == CHOICE_ROCK)) ||
        ((input == CHOICE_PAPER) && (ai_choice == CHOICE_PAPER)) ||
        ((input == CHOICE_SISSORS) && (ai_choice == CHOICE_SISSORS)))
    {
        mvwprintw(fate, 3, 1, "It's a tie!");
    }
    /* You Loose! */
    else if(((input == CHOICE_ROCK) && (ai_choice == CHOICE_PAPER)) ||
        ((input == CHOICE_PAPER) && (ai_choice == CHOICE_SISSORS)) ||
        ((input == CHOICE_SISSORS ) && ( ai_choice == CHOICE_ROCK)))
    {
        mvwprintw(fate, 3, 1, "You Lost!");
    }
    /* You Win! */
    else if(((input == CHOICE_ROCK) && (ai_choice == CHOICE_SISSORS)) ||
        ((input == CHOICE_PAPER) && (ai_choice == CHOICE_ROCK)) ||
        ((input == CHOICE_SISSORS ) && ( ai_choice == CHOICE_PAPER)))
    {
        mvwprintw(fate, 3, 1, "You Won!");
    }
    else
    {
        /* Shouldn't get here */
        mvwprintw(fate, 3, 1, "Invalid outcome!");
    }

    /* Refresh the window */
    wrefresh(fate);

    /* Wait for input from user */
    getch();

    /* Destroy the window */
    wborder(fate, ' ', ' ', ' ',' ',' ',' ',' ',' ');
    wrefresh(fate);
    delwin(fate);

    return 0;
}

int play_game(void)
{
    WINDOW *game = newwin(14, 30, 0, 0);
    int choice = 0;

    /* Initial refresh of everything */
    refresh();

    /* Draw the box for the game */
    box(game, 0, 0);


    /* Show the options to the user */
    mvwprintw(game, 1, 1, "1) Rock");
    mvwprintw(game, 2, 1, "2) Paper");
    mvwprintw(game, 3, 1, "3) Sissors");
    mvwprintw(game, 4, 1, "4) Forfeit");
    mvwprintw(game, 5, 1, "=================");
    mvwprintw(game, 6, 1, "Choose one> ");

    /* Refresh the window with the new content */
    wrefresh(game);

    /* Get input from the user */
    choice = getch();

    /* A trick to make the game border disappear */
    //wborder(game, ' ', ' ', ' ',' ',' ',' ',' ',' ');
    //wrefresh(game);
    delwin(game);

    /* Determine what the player chose */
    if ((char)choice == '1') /* Rock */
    {
        determine_fate(CHOICE_ROCK);
    }
    else if ((char)choice == '2') /* Paper */
    {
        determine_fate(CHOICE_PAPER);
    }
    else if ((char)choice == '3') /* Sissors */
    {
        determine_fate(CHOICE_SISSORS);
    }
    else if ((char)choice == '4')
    {
        //Exit the game
        return 1;
    }

    /* Return normally */
    return 0;
}

int main(int argc, char *argv[])
{
    int choice = 0;
    int is_running = 0;
    int game_choice = 0;

    /* Initialize the curses library */
    initscr();

    /* Prevents the display of what the user typed in */
    noecho();

    /* Allow the use of user input */
    keypad(stdscr, TRUE);

    /* Draws the main menu */
    do {
        choice = draw_main_menu();
        switch(choice)
        {
            case 1: /* Play Game */
            {
                /* Play the game until the user exits */
                do {
                    game_choice = play_game();

                } while(game_choice == 0);
                break;
            }
            case 2: /* Exit Game */
            {
                /* Exit the game */
                is_running = 1;
                break;
            }
        }
    } while(is_running == 0);

    /* Shut down curses when finished */
    endwin();

    /* Exit the program normally */
    return 0;
}
